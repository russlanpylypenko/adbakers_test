@extends('layouts.app')

@section('content')
    <div class="container">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br/>
        @endif
        <h1>Companies</h1>
        <a href="{{ route('company.create')}}" class="btn btn-success">Create company</a>
        <br>
        <br>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Logo</th>
                <th>Website</th>
                <th width="120px"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($companies as $company)
                <tr>
                    <td>{{$company->name}}</td>
                    <td>{{$company->email}}</td>
                    <td><img src="{{$company->getLogo()}}" alt=""></td>
                    <td><a href="{{$company->website}}">{{$company->website}}</a></td>
                    <td class="text-center">                        &nbsp;&nbsp;
                        <a class="btn btn-info btn-sm" href="{{ route('company.edit', $company->id)}}"><i class="fas fa-pen"></i></a>
                        &nbsp;&nbsp;
                        <form action="{{ route('company.destroy', $company->id)}}" method="post" style="display: inline-block">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm" type="submit"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
            {{ $companies->links() }}
    </div>
@endsection
