@extends('layouts.app')

@section('content')
    <div class="container">
        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br/>
        @endif
        <h1>Employers</h1>
        <a href="{{ route('employer.create')}}" class="btn btn-success">Create employer</a>
        <br>
        <br>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Company</th>
                <th width="120px"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($employers as $employer)
                <tr>
                    <td>{{$employer->first_name}}</td>
                    <td>{{$employer->last_name}}</td>
                    <td>{{$employer->email}}</td>
                    <td>{{$employer->phone}}</td>
                    <td>{{$employer->company->name}}</td>
                    <td class="text-center">                        &nbsp;&nbsp;
                        <a class="btn btn-info btn-sm" href="{{ route('employer.edit', $employer->id)}}"><i class="fas fa-pen"></i></a>
                        &nbsp;&nbsp;
                        <form action="{{ route('employer.destroy', $employer->id)}}" method="post" style="display: inline-block">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm" type="submit"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
            {{ $employers->links() }}
    </div>
@endsection
