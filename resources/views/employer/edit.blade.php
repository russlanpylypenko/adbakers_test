@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit Employer</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif

        <form action="{{ route('employer.update', $employer->id) }}" method="post" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="first_name">First Name:</label>
                <input type="text" class="form-control" id="first_name" name="first_name" value="{{$employer->first_name}}">
            </div>
            <div class="form-group">
                <label for="last_name">Last Name:</label>
                <input type="text" class="form-control" id="last_name" name="last_name" value="{{$employer->last_name}}">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" name="email" value="{{$employer->email}}">
            </div>

            <div class="form-group">
                <label for="phone">Phone:</label>
                <input type="text" class="form-control" id="phone" name="phone" value="{{$employer->phone}}">
            </div>
            <div class="form-group">
                <label for="company_id">Company:</label>
                <select class="form-control" id="company_id" name="company_id">
                    <option value="0">Выберите компанию...</option>
                    @foreach(\App\Company::getCompanies() as $company)
                        <option value="{{$company['id']}}"
                                @if($company['id'] === $employer->company_id) selected @endif
                                >{{$company['name']}}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>



@endsection


