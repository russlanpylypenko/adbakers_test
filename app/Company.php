<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Company extends Model
{
    protected $table = 'companies';

    protected $fillable = ['name', 'email', 'website', 'logo'];

    public function employers()
    {
        return $this->hasMany(Employer::class, 'company_id');
    }

    public function getLogo()
    {
        return asset(Storage::url($this->logo));
    }


    public static function getCompanies()
    {
        return Company::all(['id', 'name']);
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($company) {
            $company->employers()->delete();
        });
    }

}
