<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreCompanyPost;
use App\Http\Requests\UpdateCompanyPost;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::paginate(10);

        return view('company.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }


    /**
     * @param StoreCompanyPost $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreCompanyPost $request)
    {

        $filename = Storage::putFile('public', $request->file('logo'));

        $Company = new Company([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'logo' => $filename,
            'website' => $request->get('website')
        ]);

        $Company->save();


        return redirect('company')->with('success', 'Company has been added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);

        return view('company.edit', compact('company'));
    }


    public function update(UpdateCompanyPost $request, $id)
    {

        $Company = Company::find($id);
        $Company->name = $request->get('name');
        $Company->email = $request->get('email');
        $Company->website = $request->get('website');


        if ($request->file('logo')) {
            Storage::delete($Company->logo);
            $filename = Storage::putFile('public', $request->file('logo'));
            $Company->logo = $filename;
        }

        $Company->save();

        return redirect('/company')->with('success', 'Company has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Company = Company::find($id);
        $Company->delete();

        return redirect('/company')->with('success', 'Company has been deleted Successfully');
    }
}
