<?php

namespace App\Http\Controllers;

use App\Company;
use App\Employer;
use App\Http\Requests\StoreEmployerPost;
use App\Http\Requests\UpdateEmployerPost;
use Illuminate\Http\Request;

class EmployerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employers = Employer::with('company')->paginate(10);

        return view('employer.index', compact('employers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employer.create');
    }

    /**
     * @param StoreEmployerPost $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreEmployerPost $request)
    {

        $Employer = new Employer([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
        ]);

        $Company = Company::find($request->get('company_id'));
        $Employer->company()->associate($Company);

        $Employer->save();

        return redirect('employer')->with('success', 'Employer has been added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employer = Employer::find($id);

        return view('employer.edit', compact('employer'));

    }

    /**
     * @param UpdateEmployerPost $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UpdateEmployerPost $request, $id)
    {
        $Employer = Employer::find($id);
        $Employer->first_name = $request->get('first_name');
        $Employer->last_name = $request->get('last_name');
        $Employer->email = $request->get('email');
        $Employer->phone = $request->get('phone');

        $Company = Company::find($request->get('company_id'));
        $Employer->company()->associate($Company);

        $Employer->save();

        return redirect('employer')->with('success', 'Employer has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Employer = Employer::find($id);
        $Employer->delete();

        return redirect('employer')->with('success', 'Employer has been deleted Successfully');
    }
}
